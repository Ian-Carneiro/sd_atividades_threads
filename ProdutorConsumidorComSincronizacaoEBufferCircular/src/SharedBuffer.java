import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SharedBuffer implements Buffer {
    public final String NOWORD = ":NO_WORD:";
    // permite que apenas uma thread acesse os recursos
    private Lock lock = new ReentrantLock();
    // utilizado, nesta aplicação, para colocar a thread do produtor em estado de espera e retirala desse mesmo estado
    private Condition conditionToWrite = lock.newCondition();
    // utilizado, nesta aplicação, para colocar a thread do consumidor em estado de espera e retirala desse mesmo estado
    private Condition conditionToRead = lock.newCondition();

    // array onde será guardado os valores do producer
    private String[] wordBuffer = {NOWORD, NOWORD, NOWORD};
    // indica quantas posições do buffer estão ocupadas
    private int numBusyBuffers = 0;
    // indica a posição atual que deve ser lida do buffer
    private int readingIndex = 0;
    // indica a posição atual do buffer que deve ser escrita
    private int writingIndex = 0;

    // Implementação do método get da interface Buffer. Será utilizado pelo consumidor para recuperar as palavras
    @Override
    public String get() {
        String value = NOWORD;
        // quando a thread do consumidor chegar nesse ponto, a thread do produtor não poderar ter acesso aos recursos até que o lock seja liberado
        lock.lock();
        try {
            // caso a thread do consumidor obtenha o lock enquanto não existe nenhum valor no buffer ele entrará na condicional
            while(numBusyBuffers==0){
                System.out.println("[[[[[[Empty buffer. Consumer await.]]]]]]]");
                // a thread do consumidor será posta em estado de espera até que a condição seja mudada pela thread do producer
                conditionToRead.await();
            }
            // lendo o valor do buffer
            value = wordBuffer[readingIndex];
            // definindo o indice de leitura atual que pode variar de 0 a 2. A sequência respeitara a ordem: {0, 1, 2, 0, 1, ...}
            readingIndex = (readingIndex+1)%wordBuffer.length;
            // diminuindo em 1 a variável que indica a quantidade de buffers ocupados
            numBusyBuffers--;
            System.out.println("                                          Consumer: "+value);
            // a thread do producer e removida do estado de espera
            conditionToWrite.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // para casos de impasse onde os bloqueios nunca são liberados
            lock.unlock();
        }
        return value;
    }

    // Implementação do método ser da interface Buffer. Será utilizado pelo produtor para atribuir uma palavra
    @Override
    public void set(String value) {
        // quando a thread do produtor chegar nesse ponto, a thread do consumidor não poderar ter acesso aos recursos até que o lock seja liberado
        lock.lock();
        try {
            // caso a thread do produtor obtenha o lock enquanto o buffer está cheio, ele entrará na condicional
            while(wordBuffer.length == numBusyBuffers){
                System.out.println("[[[[[Full buffer. Producer await.]]]]]");
                // a thread do produtor será posta em estado de espera até que a condição seja mudada pela thread do consumer
                conditionToWrite.await();
            }
            // escrevendo no buffer
            wordBuffer[writingIndex] = value;
            // definindo próxima posição para escrever no buffer
            writingIndex = (writingIndex+1) % wordBuffer.length;
            // incrementando em 1 a variável que indica a quantidade de buffers ocupados
            numBusyBuffers++;
            System.out.println("Producer: "+value);
            // a thread do consumer foi tirada do modo de espera
            conditionToRead.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            // para casos de impasse onde os bloqueios nunca são liberados
            lock.unlock();
        }
    }
}
