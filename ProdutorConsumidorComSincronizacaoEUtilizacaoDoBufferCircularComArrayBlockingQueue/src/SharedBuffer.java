import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SharedBuffer implements Buffer {
    public final String NOWORD = ":NO_WORD:";
    // classe que implementa a estrutura de buffer circular e trata de toda a sincronização
    private ArrayBlockingQueue<String> wordBuffer;

    public SharedBuffer(){
        wordBuffer = new ArrayBlockingQueue<>(3);
    }

    // Implementação do método get da interface Buffer. Será utilizado pelo consumidor para recuperar as palavras
    @Override
    public String get() {
        String word = NOWORD;
        try {
            // pegando valor do buffer
            word = wordBuffer.take();
            System.out.println("                     Consumer: "+word);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return word;
    }

    // Implementação do método ser da interface Buffer. Será utilizado pelo produtor para atribuir uma palavra
    @Override
    public void set(String value) {
        try {
            // adicionando valor ao buffer
            wordBuffer.put(value);
            System.out.println("Producer: "+value);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
