import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main{
    // words será as palavras da frase passadas em ordem por parâmetro
    public static void main(String[] words) {
        // criando duas threads para executar o consumidor e o produtor
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        // buffer que será compartilhado pelo produtor e consumidor
        Buffer buffer = new SharedBuffer();
        try{
            // executando produtor passando o buffer e as palavras da frase como parâmetro
            executorService.execute(new Producer(buffer, words));
            // executando consumidor passando o buffer e a quantidade de palavras da frase como parâmetro
            executorService.execute(new Consumer(buffer, words.length));
        } catch (Exception e){
            e.printStackTrace();
        }
        // finalizando threads criadas
        executorService.shutdown();
    }
}