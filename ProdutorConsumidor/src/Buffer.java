public interface Buffer {
    String get();
    void set(String value);
}
