public class SharedBuffer implements Buffer {
    // Caso não exista palavra atribuida pelo produtor, o consumidor vai recuperar o valor default: "::::::no word:::::"
    public String word = "::::::no word:::::";


    // Implementação do método get da interface Buffer. Será utilizado pelo consumidor para recuperar as palavras
    @Override
    public String get() {
        System.out.println("Consumer get: "+word);
        return word;
    }

    // Implementação do método ser da interface Buffer. Será utilizado pelo produtor para atribuir uma palavra
    @Override
    public void set(String value) {
        System.out.println("Producer set:                          "+value);
        this.word = value;
    }
}
