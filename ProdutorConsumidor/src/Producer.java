import java.util.Random;

public class Producer implements Runnable {
    // utilizado para aleatorizar um valor inteiro
    private static Random random = new Random();
    // buffer que será compartilhado pelo produtor e o consumidor
    private Buffer sharedBuffer;
    // array de palavras
    private String[] words;

    public Producer(Buffer sharedBuffer, String[] words) {
        this.sharedBuffer = sharedBuffer;
        this.words = words;
    }

    @Override
    public void run() {
        // iterando sobre cada palavra do array
        for (String word: words){
            try {
                // atribuindo palavra ao buffer
                sharedBuffer.set(word);
                // fazendo a thread dormir de 0 a 3000 milisegundos
                Thread.sleep(random.nextInt(3000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Producer finished.");
    }
}
