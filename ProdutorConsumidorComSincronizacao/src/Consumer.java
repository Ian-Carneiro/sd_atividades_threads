import java.util.Random;

public class Consumer implements Runnable {
    // utilizado para aleatorizar um valor inteiro
    private static Random random = new Random();
    // buffer que será compartilhado pelo produtor e o consumidor
    private Buffer sharedBuffer;
    // quantidade de palavras que serão atribuidas ao buffer
    private int numWords;

    public Consumer(Buffer sharedBuffer, int numWords) {
        this.sharedBuffer = sharedBuffer;
        this.numWords = numWords;
    }

    @Override
    public void run() {
        // variável utilizada para concatenar e guardar as palavras da frase
        StringBuilder phrase = new StringBuilder();
        // a quantidade de iteração será igual ao valor de numWords
        for (int i=0; i<numWords; i++){
            try {
                // concatenando a palavra do buffer + " "
                phrase.append(sharedBuffer.get().concat(" "));
                // fazendo a thread dormir de 0 a 3000 milisegundos
                Thread.sleep(random.nextInt(3000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // exibindo o valor de phrase na tela
        System.out.println("Consumer finished. Formed phrase: "+phrase.toString());
    }
}
